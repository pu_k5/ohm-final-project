import {
  useLoadScript,
  GoogleMap,
  MarkerF,
  CircleF,
} from "@react-google-maps/api";
import type { NextPage } from "next";
import { useMemo, useState, useEffect } from "react";
// import usePlacesAutocomplete, {
//   getGeocode,
//   getLatLng,
// } from "use-places-autocomplete";
import styles from "../styles/Home.module.css";
import { io } from "socket.io-client";
import { Line } from "react-chartjs-2";
// import ReactApexChart from "react-apexcharts";
import dynamic from "next/dynamic";
import axios from "axios";
import { isLabeledStatement } from "typescript";
import { marksRepo } from "../helpers/marks-repo";
import Modal from "@/components/model";
import ModalComponent from "@/components/model";
import EditModal from "@/components/editModal";

const ReactApexChart = dynamic(() => import("react-apexcharts"), {
  //import กราฟ
  ssr: false,
});

// import Chart from "chart.js/auto";
// import { CategoryScale } from "chart.js";

// Chart.register(CategoryScale);

const socket = io("http://3.1.133.90:8081/", { transports: ["websocket"] }); //ดึงค่าจาก Socket

const Home: NextPage = () => {
  const [lat, setLat] = useState(27.672932021393862); //ใช้ react hook เพื่อเรียกค่า state และเปลี่ยนค่า state
  const [lng, setLng] = useState(85.31184012689732);
  const [selected, setSelected] = useState(0);
  const [avgData, setAvgData] = useState([]);
  const [click, setclick] = useState("");
  const [click1, setclick2] = useState("");
  const [coloricon, setconloricon] = useState("");
  const [isHovering, setIsHovering] = useState(false);

  const [serie10history01, setSerie10History01] = useState([{ data: [] }]);
  const [serie25history01, setSerie25History01] = useState([{ data: [] }]);

  const [serie10history02, setSerie10History02] = useState([{ data: [] }]);
  const [serie25history02, setSerie25History02] = useState([{ data: [] }]);

  const libraries = useMemo(() => ["places"], []); //useMemo เป็นการ render เพื่อไม่ให้มีการ render ซ้ำซ้อน render 1 ครั้งพอ
  const mapCenter = useMemo(() => ({ lat: lat, lng: lng }), [lat, lng]);
  const [dataList, setDatalist] = useState([]);

  const [addMarkModal, setAddMarkModal] = useState(false);
  const [editMarkModal, setEditMarkModal] = useState(false);

  const whiteContainer = useMemo(
    () => ({ lat: 16.738043, lng: 100.195036 }),
    []
  );
  const ohmRoom = useMemo(() => ({ lat: 16.751822, lng: 100.197467 }), []); // หมุดตำแหน่ง

  const [markers, setMarker] = useState([]);
  const [defaultMarker, setDefaultMarker] = useState({});

  const [editData, setEditData] = useState({
    name: "",
    position: {
      lat: null,
      lng: null,
    },
  });

  const dangerouseTextPm10 = "PM 10(121)"
  const dangerouseTextPm25 = "PM 2.5(51)"

  const getAllMarks = async () => {
    const response = await axios.get(`http://localhost:9999/mark`);
    const marks = response.data;
    console.log("markers", marks["data"]);
    setMarker(marks["data"]);
  };

  const addMark = async (data: any) => {
    await axios.post(`http://localhost:9999/mark`, data);
    getAllMarks();
  };

  const updateMark = async (data: any) => {
    await axios.put(`http://localhost:9999/mark?id=${data.id}`, data);
    getAllMarks();
  };

  const deleteMark = async (data: any) => {
    await axios.delete(`http://localhost:9999/mark?id=${data.id}`);
    getAllMarks();
  };

  const handleMapClick = (e: any) => {
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();

    setDefaultMarker({ lat: lat, lng: lng });

    setAddMarkModal(true);

    // const currentMarkers = [...markers, { position: { lat: lat, lng: lng } }];
    // setMarker(currentMarkers);
  };

  const createMarker = (data: any) => {};

  // const pm25Title: string[] = [
  //   "22",
  //   "22",
  //   "22",
  //   "22",
  //   "22",
  //   "22",
  //   "22",
  //   "22",
  //   "22",
  //   "22",
  // ];

  var pm25List_test: number[][] = [[Date.now(), 0]]; //เก็บข้อมูลค่า PM2.5 ที่วัดได้จากอุปกรณ์ โดยจะเก็บค่า timestamp ที่ข้อมูลถูกเพิ่มเข้ามาด้วย
  var pm10List_test: number[][] = [[Date.now(), 0]];

  var pm25List_10: number[][] = [[Date.now(), 0]];
  var pm10List_10: number[][] = [[Date.now(), 0]];

  var pm25List_25: number[][] = [[Date.now(), 0]];
  var pm10List_25: number[][] = [[Date.now(), 0]];

  socket.on("data", (data) => {
    if (data["_uuid"] == "PM_0001") {
      if (pm25List_10.length >= 6) {
        pm25List_10.shift(); // ถ้า array ว่างจะส่งกลับ และ คืนค่าถ้าไม่มีการแก้ไข
      }

      if (pm10List_10.length >= 6) {
        pm10List_10.shift();
      }

      pm25List_10.push([Date.now(), parseInt(data["_pm2_5"])]);
      pm10List_10.push([Date.now(), parseInt(data["_pm10"])]);
      // console.log(pm25List_10, pm10List_10);
    }

    if (data["_uuid"] == "PM_0002") {
      if (pm25List_25.length >= 6) {
        pm25List_25.shift();
      }

      if (pm10List_25.length >= 6) {
        pm10List_25.shift();
      }

      pm25List_25.push([Date.now(), parseInt(data["_pm2_5"])]);
      pm10List_25.push([Date.now(), parseInt(data["_pm10"])]);
      // console.log(pm25List_25, pm10List_25);
    }
  });

  const mapOptions = useMemo<google.maps.MapOptions>( //options กำหนดเป็น object ที่มีคุณสมบัติ ของ Google Map
    () => ({
      disableDefaultUI: true,
      clickableIcons: true,
      scrollwheel: false,
    }),
    []
  );

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: "AIzaSyDOQ0Fkk3uCGis1vz_l2cGicoLc6sAmYiY" as string, //API google Key
    libraries: libraries as any,
  });

  // const [data, updateData] = useState([1, 2, 3, 4, 5, 6]);
  // const [dataNumber, updateDataNumber] = useState(0);
  // const [titles, updateTitles] = useState("TITLE");

  var lineData_1: number[][] = []; //เป็นarray เปล่าเพื่อรอรับค่าจาก PM2.5 เพื่อไปแสดงกราฟ
  var lineData_2: number[][] = [];
  // var dataNumber = 0;

  const series_10 = [
    //ใช้เก็บข้อมูลที่นำมาแสดงผลเป็นกราฟ
    {
      data: lineData_1.slice(), //โดยเก็บข้อมูล PM2.5 ล่าสุดในตัวแปร  โดยใช้  slice() เพื่อไม่ให้ค่าที่มาใหม่ไปเปลี่ยนแปลงอันเก่า
      name: "PM10",
    },
  ];

  const series_25 = [
    {
      data: lineData_2.slice(),
      name: "PM2.5",
    },
  ];

  let series01_10_history = [
    {
      data: [{ x: 0, y: 0 }],
    },
  ];

  const series01_25_history = [
    {
      data: [{ x: 0, y: 0 }],
    },
  ];

  const series02_10_history = [
    {
      data: [{ x: 0, y: 0 }],
    },
  ];

  const series02_25_history = [
    {
      data: [{ x: 0, y: 0 }],
    },
  ];

  // const series_AVG = [
  //   {
  //     data: ,
  //   },
  // ];

  const options_10 = {
    chart: {
      id: "pm10",
      // height: 350,
      type: "line",
      animations: {
        enabled: true,
        easing: "fast",
        dynamicAnimation: {
          speed: 1000,
        },
      },
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    annotations: {
      yaxis: [
        {
          y: 51,
          borderColor: '#001aff',
          label: {
            borderColor: '#001aff',
            style: {
              color: '#fff',
              background: '#001aff'
            },
            text: dangerouseTextPm25
          }
        },
        {
          y: 121,
          borderColor: '#e30000',
          label: {
            borderColor: '#e30000',
            style: {
              color: '#fff',
              background: '#e30000'
            },
            text: dangerouseTextPm10
          }
        }
      ]
    },
    dataLabels: {
      enabled: true,
      enabledOnSeries: false,

      textAnchor: "middle",
      distributed: false,
      offsetX: 0,
      offsetY: 0,
      style: {
        fontSize: "14px",
        fontFamily: "Helvetica, Arial, sans-serif",
        fontWeight: "bold",
        colors: undefined,
      },
      background: {
        enabled: true,
        foreColor: "#fff",
        padding: 4,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: "#fff",
        opacity: 0.9,
        dropShadow: {
          enabled: false,
          top: 1,
          left: 1,
          blur: 1,
          color: "#000",
          opacity: 0.45,
        },
      },
      dropShadow: {
        enabled: false,
        top: 1,
        left: 1,
        blur: 1,
        color: "#000",
        opacity: 0.45,
      },
    },
    stroke: {
      curve: "smooth",
    },
    title: {
      text: "PM10",
      align: "left",
    },
    markers: {
      size: 0,
    },
    xaxis: {
      labels: {
        formatter: function (value: number, timestamp: number) {
          return new Date(value).toLocaleTimeString(); // The formatter function overrides format property
        },
      },
    },
    // xaxis: {
    //   labels: {
    //     show: true,
    //   },
    //   max: 6,
    // },
    yaxis: {
      min: 0,
    },
    legend: {
      show: false,
    },
  };

  const options_25 = {
    chart: {
      id: "pm25",
      // height: 350,
      type: "line",
      animations: {
        enabled: true,
        easing: "linear",
        dynamicAnimation: {
          speed: 1000,
        },
      },
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    annotations: {
      yaxis: [
        {
          y: 51,
          borderColor: '#001aff',
          label: {
            borderColor: '#001aff',
            style: {
              color: '#fff',
              background: '#001aff'
            },
            text: dangerouseTextPm25
          }
        },
        {
          y: 121,
          borderColor: '#e30000',
          label: {
            borderColor: '#e30000',
            style: {
              color: '#fff',
              background: '#e30000'
            },
            text: dangerouseTextPm10
          }
        }
      ]
    },
    dataLabels: {
      enabled: true,
      enabledOnSeries: true,

      textAnchor: "middle",
      distributed: false,
      offsetX: 0,
      offsetY: 0,
      style: {
        fontSize: "14px",
        fontFamily: "Helvetica, Arial, sans-serif",
        fontWeight: "bold",
        colors: undefined,
      },
      background: {
        enabled: true,
        foreColor: "#fff",
        padding: 4,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: "#fff",
        opacity: 0.9,
        dropShadow: {
          enabled: false,
          top: 1,
          left: 1,
          blur: 1,
          color: "#000",
          opacity: 0.45,
        },
      },
      dropShadow: {
        enabled: false,
        top: 1,
        left: 1,
        blur: 1,
        color: "#000",
        opacity: 0.45,
      },
    },
    stroke: {
      curve: "smooth",
    },
    title: {
      text: "PM2.5",
      align: "left",
    },
    markers: {
      size: 0,
    },
    xaxis: {
      labels: {
        formatter: function (value: number, timestamp: number) {
          return new Date(value).toLocaleTimeString(); // The formatter function overrides format property
        },
      },
    },
    yaxis: {
      min: 0,
    },
    legend: {
      show: false,
    },
  };

  const options10_history = {
    chart: {
      id: "pm10History",
      height: "auto",
      type: "line",
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    annotations: {
      yaxis: [
        {
          y: 51,
          borderColor: '#001aff',
          label: {
            borderColor: '#001aff',
            style: {
              color: '#fff',
              background: '#001aff'
            },
            text: dangerouseTextPm25
          }
        },
        {
          y: 121,
          borderColor: '#e30000',
          label: {
            borderColor: '#e30000',
            style: {
              color: '#fff',
              background: '#e30000'
            },
            text: dangerouseTextPm10
          }
        }
      ]
    },
    dataLabels: {
      enabled: false,
      enabledOnSeries: false,

      textAnchor: "middle",
      distributed: false,
      offsetX: 0,
      offsetY: 0,
      style: {
        fontSize: "14px",
        fontFamily: "Helvetica, Arial, sans-serif",
        fontWeight: "bold",
      },
      background: {
        enabled: true,
        foreColor: "#fff",
        padding: 4,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: "#fff",
        opacity: 0.9,
        dropShadow: {
          enabled: false,
          top: 1,
          left: 1,
          blur: 1,
          color: "#000",
          opacity: 0.45,
        },
      },
      dropShadow: {
        enabled: false,
        top: 1,
        left: 1,
        blur: 1,
        color: "#000",
        opacity: 0.45,
      },
    },
    markers: {
      size: 0,
    },
    title: {
      text: "PM10",
      align: "left",
    },
    yaxis: {
      min: 0,
    },
    tooltip: {
      shared: true,
    },
    legend: {
      show: false,
    },
  };

  const options25_history = {
    chart: {
      id: "pm25History",
      height: "auto",
      type: "line",
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    annotations: {
      yaxis: [
        {
          y: 51,
          borderColor: '#001aff',
          label: {
            borderColor: '#001aff',
            style: {
              color: '#fff',
              background: '#001aff'
            },
            text: dangerouseTextPm25
          }
        },
        {
          y: 121,
          borderColor: '#e30000',
          label: {
            borderColor: '#e30000',
            style: {
              color: '#fff',
              background: '#e30000'
            },
            text: dangerouseTextPm10
          }
        }
      ]
    },
    dataLabels: {
      enabled: false,
      enabledOnSeries: false,

      textAnchor: "middle",
      distributed: false,
      offsetX: 0,
      offsetY: 0,
      style: {
        fontSize: "14px",
        fontFamily: "Helvetica, Arial, sans-serif",
        fontWeight: "bold",
      },
      background: {
        enabled: true,
        foreColor: "#fff",
        padding: 4,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: "#fff",
        opacity: 0.9,
        dropShadow: {
          enabled: false,
          top: 1,
          left: 1,
          blur: 1,
          color: "#000",
          opacity: 0.45,
        },
      },
      dropShadow: {
        enabled: false,
        top: 1,
        left: 1,
        blur: 1,
        color: "#000",
        opacity: 0.45,
      },
    },
    markers: {
      size: 0,
    },
    title: {
      text: "PM25",
      align: "left",
    },
    yaxis: {
      min: 0,
    },
    legend: {
      show: false,
    },
  };

  const FetchAverageWeek = async () => {
    // fetch ข้อมูลจาก API
    try {
      console.log("Fetch Average");
      const response = await axios.get(`http://localhost:9999/avg_week`); //API จาก server.js เป็น API Database
      const resp = response.data;

      const data10Avg01: { x: any; y: any }[] = [];
      const data10Max01: { x: any; y: any }[] = [];
      const data10Min01: { x: any; y: any }[] = [];

      const data25Avg01: { x: any; y: any }[] = [];
      const data25Max01: { x: any; y: any }[] = [];
      const data25Min01: { x: any; y: any }[] = [];

      const data10Avg02: { x: any; y: any }[] = [];
      const data10Max02: { x: any; y: any }[] = [];
      const data10Min02: { x: any; y: any }[] = [];

      const data25Avg02: { x: any; y: any }[] = [];
      const data25Max02: { x: any; y: any }[] = [];
      const data25Min02: { x: any; y: any }[] = [];

      resp.map((data: any) => {
        const avg10Value = {
          x: data["_id"]["minute"],
          y: Number(data["average_pm10"]).toFixed(2),
        };
        const max10Value = {
          x: data["_id"]["minute"],
          y: Number(data["max_pm10"]).toFixed(2),
        };
        const min10Value = {
          x: data["_id"]["minute"],
          y: Number(data["min_pm10"]).toFixed(2),
        };

        const avg25Value = {
          x: data["_id"]["minute"],
          y: Number(data["average_pm2_5"]).toFixed(2),
        };
        const max25Value = {
          x: data["_id"]["minute"],
          y: Number(data["max_pm2_5"]).toFixed(2),
        };
        const min25Value = {
          x: data["_id"]["minute"],
          y: Number(data["min_pm2_5"]).toFixed(2),
        };

        if (data["_id"]["uuid"] == "PM_0001") {
          data10Avg01.push(avg10Value);
          data10Max01.push(max10Value);
          data10Min01.push(min10Value);

          data25Avg01.push(avg25Value);
          data25Max01.push(max25Value);
          data25Min01.push(min25Value);

          console.log("AVG10", avg10Value);
          console.log("MAX10", max10Value);
          console.log("MIN10", min10Value);

          console.log("AVG25", avg25Value);
          console.log("MAX25", max25Value);
          console.log("MIN25", min25Value);
        } else if (data["_id"]["uuid"] == "PM_0002") {
          data10Avg02.push(avg10Value);
          data10Max02.push(max10Value);
          data10Min02.push(min10Value);

          data25Avg02.push(avg25Value);
          data25Max02.push(max25Value);
          data25Min02.push(min25Value);
        }
      });

      const all10List01 = [
        { data: data10Avg01, name: "AVG" },
        { data: data10Max01, name: "MAX" },
        { data: data10Min01, name: "MIN" },
      ];

      const all25List01 = [
        { data: data25Avg01, name: "AVG" },
        { data: data25Max01, name: "MAX" },
        { data: data25Min01, name: "MIN" },
      ];

      const all10List02 = [
        { data: data10Avg02, name: "AVG" },
        { data: data10Max02, name: "MAX" },
        { data: data10Min02, name: "MIN" },
      ];

      const all25List02 = [
        { data: data25Avg02, name: "AVG" },
        { data: data25Max02, name: "MAX" },
        { data: data25Min02, name: "MIN" },
      ];

      setSerie10History01(all10List01);
      setSerie25History01(all25List01);

      setSerie10History02(all10List02);
      setSerie25History02(all25List02);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    const FetchAverage = async () => {
      // fetch ข้อมูลจาก API
      try {
        const response = await axios.get(`http://localhost:9999/avg`); //API จาก server.js เป็น API Database
        setAvgData(response.data); //เก็บค่าDataโดยการ setAvgData เพื่อดึง hook เก็บค่าลง state
        console.log('Average', response.data);
      } catch (err) {
        console.error(err);
      }
    };
    FetchAverage();
    FetchAverageWeek();

    getAllMarks();
  }, []);

  const handleAverageData = (uuid: string, onlyLabel: boolean) => {
    //หาค่าเฉลี่ยของ PM2.5 จากข้อมูลที่มีอยู่และใช้ uuid เป็นตัวกำหนดข้อมูลที่ต้องการค้นหาค่าเฉลี่ย
    const found: any = avgData.find((obj) => obj["_id"] === uuid);
    if (!onlyLabel) {
      return found;
    }
    const pmavg25: number = found?.avg25;
    return Math.round(pmavg25);
  };

  useEffect(() => {
    const interval = setInterval(() => {
      const val = Math.floor(Math.random() * (100 - 30 + 1)) + 30; //test
      const val2 = Math.floor(Math.random() * (100 - 30 + 1)) + 30;

      pm10List_test.push([Date.now(), val]);
      pm25List_test.push([Date.now(), val2]);

      if (selected == 1) {
        lineData_1 = pm10List_10.slice(Math.max(pm10List_10.length - 5, 0));
        lineData_2 = pm25List_10.slice(Math.max(pm25List_10.length - 5, 0));
      } else if (selected == 2) {
        lineData_1 = pm10List_25.slice(Math.max(pm10List_25.length - 5, 0));
        lineData_2 = pm25List_25.slice(Math.max(pm25List_25.length - 5, 0));
      } else {
        lineData_1 = pm10List_test.slice(Math.max(pm10List_test.length - 5, 0));
        lineData_2 = pm25List_test.slice(Math.max(pm25List_test.length - 5, 0));
      }

      // if (pm25List_10.length > 6) {
      //   pm25List_10.shift();
      // }

      // if (pm10List_10.length > 6) {
      //   pm10List_10.shift();
      // }

      // if (pm25List_25.length > 6) {
      //   pm25List_25.shift();
      // }

      // if (pm10List_25.length > 6) {
      //   pm10List_25.shift();
      // }

      if (lineData_1.length > 6) {
        // lineData_1.shift();
        lineData_1.shift();
      }

      if (lineData_2.length > 6) {
        // lineData_2.shift();
        lineData_2.shift();
      }

      ApexCharts.exec("pm10", "updateSeries", [
        {
          data: lineData_1.slice(Math.max(lineData_1.length - 5, 0)),
        },
      ]);
      ApexCharts.exec("pm25", "updateSeries", [
        {
          data: lineData_2.slice(Math.max(lineData_2.length - 5, 0)),
        },
      ]);
    }, 3000);

    return () => {
      window.clearInterval(interval); // clear the interval in the cleanup function
      window.dispatchEvent(new Event("resize"));
      ``;
    };
  }, [lineData_1, lineData_2, selected]);

  // useEffect(() => {
  //   if (selected == 1) {
  //     lineData_1 = pm10List_10.slice(Math.max(pm10List_10.length - 5, 0));
  //     lineData_2 = pm25List_10.slice(Math.max(pm25List_10.length - 5, 0));
  //   } else if (selected == 2) {
  //     lineData_1 = pm10List_25.slice(Math.max(pm10List_25.length - 5, 0));
  //     lineData_2 = pm25List_25.slice(Math.max(pm25List_25.length - 5, 0));
  //   } else {
  //     lineData_1 = pm10List_test.slice(Math.max(pm10List_test.length - 5, 0));
  //     lineData_2 = pm25List_test.slice(Math.max(pm25List_test.length - 5, 0));
  //   }
  // }, [
  //   pm10List_10,
  //   pm25List_10,
  //   pm10List_25,
  //   pm25List_25,
  //   pm10List_test,
  //   pm25List_test,
  // ]);

  if (!isLoaded) {
    return <p>Loading...</p>;
  }

  // let PM_02 : number = 51;

  // if(PM_02 >= pm10List_test){

  //  }

  const iconBase =
    "https://developers.google.com/maps/documentation/javascript/examples/full/images/";

  return (
    <>
      <div>
        <div className={styles.homeWrapper}>
          <div className={styles.sidebar}>
            <div>
              <p style={{ color: "black", fontSize: 20, fontWeight: "bolder" }}>
                {" "}
                {click}{" "}
              </p>
              {/* <Line data={data} width={400} height={400} options={options} /> */}
              <ReactApexChart
                options={options_10}
                series={series_10}
                type="line"
              />
              <ReactApexChart
                options={options_25}
                series={series_25}
                type="line"
              />
              {/* <img src="https://medias.thansettakij.com/uploads/images/contents/w1024/2023/02/8vfscJId1XLQNdwVanG5.webp" width={450} height={450} alt="Example Image"  /> */}
            </div>
            {/* <div>
            <div className={styles.text}>{dataNumber}</div>
            <ul>
              {lineData.map((number) => (
                <li className={styles.text}>{number}</li>
              ))}
            </ul>
          </div> */}
          </div>
          <GoogleMap
            options={mapOptions}
            zoom={14}
            center={whiteContainer}
            mapTypeId={google.maps.MapTypeId.ROADMAP}
            mapContainerStyle={{ width: "150vw", height: "100vh" }}
            onLoad={(map) => console.log("Map Loaded")}
            onClick={handleMapClick}
          >
            {avgData.length != 0 && (
              //   <MarkerF
              //     position={whiteContainer}
              //     onLoad={() => console.log("Marker Loaded")}
              //     onClick={() => {
              //       setSelected(1);
              //       // lineData_1 = pm10List_10.slice(
              //       //   Math.max(pm10List_10.length - 5, 0)
              //       // );
              //       // lineData_2 = pm25List_10.slice(
              //       //   Math.max(pm25List_10.length - 5, 0)
              //       // );
              //       // console.log(lineData_1, lineData_2);
              //       // console.log(handleAverageData("PM_0001", true));
              //     }}
              //     title={"PM_0001"}
              //     icon={{
              //       // path: google.maps.SymbolPath.CIRCLE,
              //       url: iconBase + "info-i_maps.png",
              //       fillColor: "#EB00FF",
              //       scale: 7,
              //     }}
              //   />
              // ) : (

              <MarkerF
                position={whiteContainer}
                onLoad={() => console.log("Marker Loaded")}
                onClick={() => {
                  setSelected(2);
                  setclick("วิทยาลัยพลังงาน");
                  setclick2("ค่าPM");

                  // lineData_1 = pm10List_10.slice(
                  //   Math.max(pm10List_10.length - 5, 0)
                  // );
                  // lineData_2 = pm25List_10.slice(
                  //   Math.max(pm25List_10.length - 5, 0)
                  // );
                  // console.log(lineData_1, lineData_2);
                  // console.log(handleAverageData("PM_0001", true));
                }}
                title={"วิทยาลัยพลังงาน"} //ชี้ว่าจุดนี้ชื่ออะไร
                icon={{
                  path: google.maps.SymbolPath.CIRCLE,
                  url: iconBase + "info-i_maps.png",
                  fillColor: "#33FF66",
                }}
                label={{
                  text: `${handleAverageData("PM_0002", true)}`,
                  color: "#000",
                  fontSize: "50",
                  fontWeight: "bold",
                }}
              />
            )}

            {avgData.length != 0 && (
              //   <MarkerF
              //     position={ohmRoom}
              //     onLoad={() => console.log("Marker Loaded")}
              //     onClick={() => {
              //       setSelected(2);
              //       // lineData_1 = pm10List_25.slice(
              //       //   Math.max(pm10List_25.length - 5, 0)
              //       // );
              //       // lineData_2 = pm25List_25.slice(
              //       //   Math.max(pm25List_25.length - 5, 0)
              //       // );
              //       // console.log(lineData_1, lineData_2);
              //       // console.log(handleAverageData("PM_0002", true));
              //     }}
              //     title={"PM_0002"}
              //     icon={{
              //       // path: google.maps.SymbolPath.CIRCLE,
              //       url: iconBase + "info-i_maps.png",
              //       fillColor: "#EB00FF",
              //       scale: 7,
              //     }}
              //   />
              // ) : (
              <MarkerF
                position={ohmRoom}
                onLoad={() => console.log("Marker Loaded")}
                onClick={() => {
                  setSelected(2);
                  setclick("หอพักของฉัน");

                  // lineData_1 = pm10List_25.slice(
                  //   Math.max(pm10List_25.length - 5, 0)
                  // );
                  // lineData_2 = pm25List_25.slice(
                  //   Math.max(pm25List_25.length - 5, 0)
                  // );
                  // console.log(lineData_1, lineData_2);
                  // console.log(handleAverageData("PM_0002", true));
                }}
                title={"หอพักของฉัน"}
                icon={{
                  path: google.maps.SymbolPath.CIRCLE,

                  url: iconBase + "info-i_maps.png",
                  fillColor: "#33FF66",

                  scale: 7,
                }}
                label={{
                  text: `${handleAverageData("PM_0001", true)}`,
                  color: "#000",
                  fontSize: "100",
                  fontWeight: "bold",
                }}
              />
            )}

            <MarkerF
              position={{ lat: 16.746483, lng: 100.177471 }}
              onLoad={() => console.log("Marker Loaded")}
              onClick={() => {
                setSelected(0);

                // lineData_1 = pm10List_test.slice(
                //   Math.max(pm10List_test.length - 5, 0)
                // );
                // lineData_2 = pm25List_test.slice(
                //   Math.max(pm25List_test.length - 5, 0)
                // );
                // console.log(lineData_1, lineData_2);
              }}
              title={"PM_TEST"}
              icon={{
                path: "M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z",
                fillColor: "green",
                fillOpacity: 0.5,
                strokeWeight: 0,
                rotation: 0,
                scale: 1,
              }}
              label={{
                text: `${handleAverageData("NaN", true)}`,
                color: "#000",
                fontSize: "100",
                fontWeight: "bold",
              }}
            />

            {markers.map((marker) => (
              <MarkerF
                position={marker.position}
                title={marker.name}
                onLoad={() => console.log("Marker Loaded")}
                onClick={() => {
                  setEditData(marker);
                  setEditMarkModal(true);
                }}
                icon={{
                  path: google.maps.SymbolPath.CIRCLE,

                  url: iconBase + "info-i_maps.png",
                  fillColor: "#33FF66",

                  scale: 7,
                }}
              />
            ))}
          </GoogleMap>

          <Modal
            isOpen={addMarkModal}
            defaultData={defaultMarker}
            callBack={(_data: any) => {
              console.log("Mark", _data);
              // const data = JSON.parse(_data);
              const json = {
                name: _data.name,
                position: {
                  lat: defaultMarker.lat,
                  lng: defaultMarker.lng,
                },
              };

              addMark(json);

              console.log(json);
              setAddMarkModal(false);
            }}
            onClose={function (): void {
              setAddMarkModal(false);
            }}
          />

          <EditModal
            isOpen={editMarkModal}
            defaultData={editData}
            callBack={(_data: any) => {
              console.log("Mark", _data);

              const json = {
                id: _data.id,
                name: _data.name,
                position: {
                  lat: parseFloat(_data.lat),
                  lng: parseFloat(_data.lng),
                },
              };

              updateMark(json);
              setEditMarkModal(false);
            }}
            onClose={function (): void {
              setEditMarkModal(false);
            }}
            onDelete={(_data: any) => {
              deleteMark(_data);
              setEditMarkModal(false);
            }}
          />
        </div>
        <div>
          <div
            style={{
              position: "absolute",
              backgroundColor: "white",
              display: "flex",
              justifyContent: "space-around",
              bottom: 0,
              width: "100%",
              height: "40%",
            }}
          >
            <div style={{ width: "40%" }}>
              <h1 style={{ color: "black" }}>PM_001 7 Days</h1>
              <div style={{ display: "flex" }}>
                <ReactApexChart
                  height="300px"
                  options={options10_history}
                  series={serie10history01}
                  type="line"
                />
                <ReactApexChart
                  height="300px"
                  options={options25_history}
                  series={serie25history01}
                  type="line"
                />
              </div>
            </div>

            <div style={{ width: "40%" }}>
              <h1 style={{ color: "black" }}>PM_002 7 days</h1>
              <div style={{ display: "flex" }}>
                <ReactApexChart
                  height="300px"
                  options={options10_history}
                  series={serie10history02}
                  type="line"
                />
                <ReactApexChart
                  height="300px"
                  options={options25_history}
                  series={serie25history02}
                  type="line"
                />
              </div>
            </div>

            <div style={{ width: "20%" }}>
              <div>
                <p>
                  <a href="http://air4thai.pcd.go.th/webV2/aqi_info.php#:~:text=%E0%B8%94%E0%B8%B1%E0%B8%8A%E0%B8%99%E0%B8%B5%E0%B8%84%E0%B8%B8%E0%B8%93%E0%B8%A0%E0%B8%B2%E0%B8%9E%E0%B8%AD%E0%B8%B2%E0%B8%81%E0%B8%B2%E0%B8%A8%20(Air%20Quality,%E0%B9%80%E0%B8%82%E0%B9%89%E0%B8%A1%E0%B8%82%E0%B9%89%E0%B8%99%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B8%AA%E0%B8%B2%E0%B8%A3%E0%B8%A1%E0%B8%A5%E0%B8%9E%E0%B8%B4%E0%B8%A9%E0%B8%97%E0%B8%B2%E0%B8%87">
                    <img
                      src="https://medias.thansettakij.com/uploads/images/contents/w1024/2023/02/8vfscJId1XLQNdwVanG5.webp"
                      style={{ width: "100%" }}
                      alt="Example Image"
                      title={"Click to see more"}
                    />
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
