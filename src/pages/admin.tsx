import type { NextPage } from "next";
import { marksRepo } from "../helpers/marks-repo";
import { useMemo, useState } from "react";
import {
  useLoadScript,
  GoogleMap,
  MarkerF,
  CircleF,
} from "@react-google-maps/api";
import Modal from "@/components/model";

const Admin: NextPage = () => {
  const libraries = useMemo(() => ["places"], []); //useMemo เป็นการ render เพื่อไม่ให้มีการ render ซ้ำซ้อน render 1 ครั้งพอ
  const iconBase =
    "https://developers.google.com/maps/documentation/javascript/examples/full/images/";

  const ohmRoom = useMemo(() => ({ lat: 16.751822, lng: 100.197467 }), []); // หมุดตำแหน่ง

  const whiteContainer = useMemo(
    () => ({ lat: 16.738043, lng: 100.195036 }),
    []
  );

  const [markers, setMarker] = useState([
    { position: { lat: 16.74112525642241, lng: 100.16993052362058 } },
  ]);

  const handleMapClick = (e: any) => {
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();

    marksRepo.create({ lat: lat, lng: lng });
    console.log(marksRepo.getAll());

    const currentMarkers = [...markers, { position: { lat: lat, lng: lng } }];
    setMarker(currentMarkers);
  };

  const mapOptions = useMemo<google.maps.MapOptions>( //options กำหนดเป็น object ที่มีคุณสมบัติ ของ Google Map
    () => ({
      disableDefaultUI: true,
      clickableIcons: true,
      scrollwheel: false,
    }),
    []
  );

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: "AIzaSyDOQ0Fkk3uCGis1vz_l2cGicoLc6sAmYiY" as string, //API google Key
    libraries: libraries as any,
  });

  if (!isLoaded) {
    return <p>Loading...</p>;
  }

  return (
    <>
      <div>
        <GoogleMap
          options={mapOptions}
          zoom={14}
          center={whiteContainer}
          mapTypeId={google.maps.MapTypeId.ROADMAP}
          mapContainerStyle={{ width: "150vw", height: "100vh" }}
          onLoad={(map) => console.log("Map Loaded")}
          onClick={handleMapClick}
        >
          <MarkerF
            position={whiteContainer}
            onLoad={() => console.log("Marker Loaded")}
            title={"วิทยาลัยพลังงาน"} //ชี้ว่าจุดนี้ชื่ออะไร
            icon={{
              path: google.maps.SymbolPath.CIRCLE,
              url: iconBase + "info-i_maps.png",
              fillColor: "#33FF66",
            }}
          />

          <MarkerF
            position={ohmRoom}
            onLoad={() => console.log("Marker Loaded")}
            title={"หอพักของฉัน"}
            icon={{
              path: google.maps.SymbolPath.CIRCLE,

              url: iconBase + "info-i_maps.png",
              fillColor: "#33FF66",

              scale: 7,
            }}
          />

          <MarkerF
            position={{ lat: 16.746483, lng: 100.177471 }}
            onLoad={() => console.log("Marker Loaded")}
            title={"PM_TEST"}
            icon={{
              path: "M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z",
              fillColor: "green",
              fillOpacity: 0.5,
              strokeWeight: 0,
              rotation: 0,
              scale: 1,
            }}
          />

          {markers.map((marker) => (
            <MarkerF
              position={marker.position}
              onLoad={() => console.log("Marker Loaded")}
              title={"วิทยาลัยพลังงาน"} //ชี้ว่าจุดนี้ชื่ออะไร
              onClick={() => {}}
              icon={{
                path: google.maps.SymbolPath.CIRCLE,

                url: iconBase + "info-i_maps.png",
                fillColor: "#33FF66",

                scale: 7,
              }}
            />
          ))}
        </GoogleMap>
        <Modal isOpen={true}>
          <h2>Modal Content</h2>
          <p>This is the content of the modal.</p>
        </Modal>
      </div>
    </>
  );
};

export default Admin;
