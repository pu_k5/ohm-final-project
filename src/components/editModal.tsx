import React, { FC, useState, useEffect } from "react";

interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
  callBack: Function;
  defaultData: any;
  onDelete: Function;
}

const EditModal: FC<ModalProps> = ({
  isOpen,
  onClose,
  callBack,
  defaultData,
  onDelete,
}) => {
  const [formData, setFormData] = useState({
    id: "",
    name: "",
    lat: 0,
    lng: 0,
  });

  useEffect(() => {
    const setValue = async () => {
      console.log("sue", defaultData);

      setFormData({
        id: await defaultData.id,
        name: await defaultData.name,
        lat: await defaultData.position.lat,
        lng: await defaultData.position.lng,
      });
    };

    setValue();
  }, [isOpen]);

  if (!isOpen) return null;

  const handleInput = (e: { target: { name: any; value: any } }) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;

    setFormData((prevState) => ({
      ...prevState,
      [fieldName]: fieldValue,
    }));
  };

  const submitForm = (e: {
    preventDefault: () => void;
    target: { action: any };
  }) => {
    // We don't want the page to refresh
    e.preventDefault();

    callBack(formData);
  };

  return (
    <div
      style={{
        display: "flex",
        position: "absolute",
        padding: 40,
        borderRadius: 10,
        backgroundColor: "white",
        borderWidth: 2,
        borderColor: "black",
        paddingBlock: 50,
      }}
    >
      <div className="modal-overlay">
        <div className="modal">
          <div className="modal-content">
            <form onSubmit={submitForm}>
              <div>
                <label>Name : </label>
                <input
                  style={{
                    width: "100%",
                    borderRadius: 5,
                    height: 30,
                    background: "#fff",
                  }}
                  type="text"
                  name="name"
                  onChange={handleInput}
                  value={formData.name}
                />
              </div>

              <div style={{ marginTop: 20, marginBottom: 20 }}>
                <label>Lat : </label>
                <input
                  style={{
                    borderRadius: 5,
                    height: 30,
                    background: "#fff",
                  }}
                  type="text"
                  name="lat"
                  value={formData.lat}
                  onChange={handleInput}
                />
                <label>Lng : </label>
                <input
                  style={{
                    borderRadius: 5,
                    height: 30,
                    background: "#fff",
                  }}
                  type="text"
                  name="lng"
                  value={formData.lng}
                  onChange={handleInput}
                />
              </div>

              <button style={{ width: 100, height: 30 }} type="submit">
                Edit
              </button>
            </form>

            <button
              style={{
                width: 100,
                height: 30,
                background: "red",
                marginTop: 10,
              }}
              onClick={() => {
                onDelete(formData);
              }}
            >
              Delete
            </button>

            <button
              style={{
                width: 100,
                height: 30,
                marginTop: 10,
              }}
              onClick={() => {
                onClose();
              }}
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditModal;
