import React, { FC, useState } from "react";

interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
  callBack: Function;
  defaultData: any;
}

const Modal: FC<ModalProps> = ({ isOpen, onClose, callBack, defaultData }) => {
  console.log("Modal", defaultData);

  const [formData, setFormData] = useState({
    name: "",
    lat: defaultData.lat,
    lng: defaultData.lng,
  });

  if (!isOpen) return null;

  const handleInput = (e: { target: { name: any; value: any } }) => {
    const fieldName = e.target.name;
    const fieldValue = e.target.value;

    setFormData((prevState) => ({
      ...prevState,
      [fieldName]: fieldValue,
    }));
  };

  const submitForm = (e: {
    preventDefault: () => void;
    target: { action: any };
  }) => {
    // We don't want the page to refresh
    e.preventDefault();

    // const data = new FormData();

    // // Turn our formData state into data we can use with a form submission
    // Object.entries(formData).forEach(([key, value]) => {
    //   data.append(key, value);
    //   console.log(key, value);
    // });

    // let formDataObject = Object.fromEntries(data.entries());
    // // Format the plain form data as JSON
    // let formDataJsonString = JSON.stringify(formDataObject);

    callBack(formData);
  };

  return (
    <div
      style={{
        display: "flex",
        position: "absolute",
        padding: 40,
        borderRadius: 10,
        backgroundColor: "white",
        borderWidth: 2,
        borderColor: "black",
      }}
    >
      <div className="modal-overlay">
        <div className="modal">
          <div className="modal-content">
            <form onSubmit={submitForm}>
              <div>
                <label>Name : </label>
                <input
                  style={{
                    width: "100%",
                    borderRadius: 5,
                    height: 30,
                    background: "#fff",
                  }}
                  type="text"
                  name="name"
                  onChange={handleInput}
                  value={formData.name}
                />
              </div>

              {/* <div>
                <label>Lat : </label>
                <input
                  type="text"
                  name="lat"
                  value={formData.lat}
                  onChange={handleInput}
                />
                <label>Lng : </label>
                <input
                  type="text"
                  name="lng"
                  value={formData.lng}
                  onChange={handleInput}
                />
              </div> */}

              <button
                style={{ width: 100, height: 30, marginTop: 10 }}
                type="submit"
              >
                Save
              </button>
            </form>

            <button
              style={{ width: 100, height: 30, marginTop: 10 }}
              onClick={() => {
                onClose();
              }}
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
