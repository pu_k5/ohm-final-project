// import fs from 'fs/promises';
// import { promises as fs } from 'fs';

// const fs = require('fs')

// import path from 'path';
// marks in JSON file for simplicity, store in a db for production applications
// let marks = require('./marks.json');

// let marks = path.join(process.cwd(), './data/marks.json');

let marks = []

export const marksRepo = {
    getAll: () => marks,
    getById: id => marks.find(x => x.id.toString() === id.toString()),
    create,
    update,
    delete: _delete
};

function create(user) {
    // generate new mark id
    user.id = marks.length ? Math.max(...marks.map(x => x.id)) + 1 : 1;

    // set date created and updated
    user.dateCreated = new Date().toISOString();
    user.dateUpdated = new Date().toISOString();

    // add and save mark
    marks.push(user);
    saveData();
}

function update(id, params) {
    const user = marks.find(x => x.id.toString() === id.toString());

    // set date updated
    user.dateUpdated = new Date().toISOString();

    // update and save
    Object.assign(user, params);
    // saveData();
}

// prefixed with underscore '_' because 'delete' is a reserved word in javascript
function _delete(id) {
    // filter out deleted user and save
    marks = marks.filter(x => x.id.toString() !== id.toString());
    saveData();
    
}

// private helper functions

async function saveData() {
    // const fileHandle = await fs.open('marks.json', 'w');
    fs.writeFile('./marks.json', 'Hello, world');
}

