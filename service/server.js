const express = require("express");
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");
const res = require("express/lib/response");
const fs = require("fs");
const bodyParser = require("body-parser");

let rawdata = fs.readFileSync('service/marks.json');
let marks = JSON.parse(rawdata);

app.use(cors());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);

mongoose.connect(
  "mongodb+srv://ohmohmohma30412:ohm0966477158@ohm1918.maf3klm.mongodb.net/JJ?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
  }
);

//สร้าง database schema
const Premium = mongoose.model(
  "wits",
  new mongoose.Schema({ _uuid: String, _pm2_5: String })
);

app.get("/", (req, res) => {
  res.json({ message: "ok" });
});

app.get("/wits", (req, res) => {
  Premium.find({})
    .then((wits) => res.json(wits))
    .catch((error) =>
      res.status(400).json({ message: "Some thing went wrong" })
    );
});

app.get("/wits/1", (req, res) => {
  Premium.find({ _uuid: "PM_0001" })
    .then((wits) => res.json(wits))
    .catch((error) =>
      res.status(400).json({ message: "Some thing went wrong" })
    );
});

app.get("/wits/2", (req, res) => {
  const now = new Date();
  const lastHour = now.setHours(now.getHours() - 1);
  Premium.find({
    _uuid: "PM_0002",
    _time: { $gte: lastHour, $lte: Date.now() },
  })
    .then((wits) => res.json(wits))
    .catch((error) =>
      res.status(400).json({ message: "Some thing went wrong" })
    );
});

app.get("/avg", (req, res) => {
  const now = new Date();
  const lastHour = now.setHours(now.getHours() - 1);

  Premium.aggregate([
    {
      $match: { _time: { $gte: lastHour, $lte: Date.now() } },
    },
    {
      $group: {
        _id: "$_uuid",
        avg25: { $avg: { $toInt: "$_pm2_5" } },
        avg10: { $avg: { $toInt: "$_pm10" } },
      },
    },
  ])
    .then((result) => res.json(result))
    .catch((error) =>
      res.status(400).json({ message: "Some thing went wrong" })
    );
});

app.get("/avg_week", (req, res) => {
  const now = new Date();
  const lastDate = now.setDate(now.getDate() - 7);
  console.log(lastDate);
  Premium.aggregate([
    {
      $match: { _time: { $gte: lastDate, $lte: Date.now() } },
    },
    {
      $group: {
        _id: {
          uuid: "$_uuid",
          minute: {
            $dateToString: {
              format: "%Y-%m-%d",
              date: { $toDate: "$_time" },
            },
          },
        },
        average_pm2_5: {
          $avg: { $convert: { input: "$_pm2_5", to: "double", onError: 0 } },
        },
        average_pm10: {
          $avg: { $convert: { input: "$_pm10", to: "double", onError: 0 } },
        },
        min_pm2_5: {
          $min: { $convert: { input: "$_pm2_5", to: "double", onError: 0 } },
        },
        max_pm2_5: {
          $max: { $convert: { input: "$_pm2_5", to: "double", onError: 0 } },
        },
        min_pm10: {
          $min: { $convert: { input: "$_pm10", to: "double", onError: 0 } },
        },
        max_pm10: {
          $max: { $convert: { input: "$_pm10", to: "double", onError: 0 } },
        },
      },
    },
    {
      $sort: { "_id.minute": 1 },
    },
  ])
    .then((result) => res.json(result))
    .catch((error) =>
      res.status(400).json({ message: "Some thing went wrong" })
    );
});

app.get("/mark", (req, res) => {
  res.status(200).json({ message: "Get mark successful", data: marks })
});

app.post("/mark", (req, res) => {
  const body = req.body;
  console.log('req.body: ', req.body);
  create(body);

  res.status(201).json({ message: "Add mark successful", data: body })
});

app.put("/mark", (req, res) => {
  const id = req.query.id;
  const body = req.body;
  console.log('req.body: ', req.body);
  update(id,body);

  res.status(201).json({ message: "Update mark successful", data: body })
});

app.delete("/mark", (req, res) => {
  const id = req.query.id;
  console.log('id: ', id);
  _delete(id)

  res.status(200).json({ message: `Delete mark id ${id} successful` })
});

function create(mark) {
  // generate new mark id
  mark.id = marks.length ? Math.max(...marks.map((x) => x.id)) + 1 : 1;

  // set date created and updated
  mark.dateCreated = new Date().toISOString();
  mark.dateUpdated = new Date().toISOString();

  // add and save mark
  marks.push(mark);
  saveData();
}

function update(id, params) {
  const mark = marks.find(x => x.id.toString() === id.toString());

  // set date updated
  mark.dateUpdated = new Date().toISOString();

  // update and save
  Object.assign(mark, params);
  saveData();
}

function _delete(id) {
  // filter out deleted user and save
  marks = marks.filter(x => x.id.toString() !== id.toString());
  saveData();
  
}

function saveData() {
  fs.writeFileSync("service/marks.json", JSON.stringify(marks, null, 4));
}

app.listen(9999, () => console.log("ok"));
